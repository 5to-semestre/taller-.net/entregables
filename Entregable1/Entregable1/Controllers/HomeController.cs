﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Entregable1.Models;

namespace Entregable1.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }
        // Carga formulario para Anotar.
        public ActionResult VistaAnotar()
        {
            return View();
        }
        // Lógica de anotar y carga vista de confirmación de anotar.
        public ActionResult Anotar()
        {
            string nombre = Request.Form["nombre"].ToString();
            string telefono = Request.Form["telefono"].ToString();
            string fechaNac = Request.Form["fecha"].ToString();
            string comentario = Request.Form["comentario"].ToString();
            Contacto c = new Contacto(nombre, telefono, fechaNac, comentario);
            X_Document.agregar(c);
            return View();
        }
        public ActionResult Listar()
        {
            ViewData["contactos"] = X_Document.listar();
            return View();
        }
    }
}