﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Web.Hosting;
using System.Configuration;

namespace Entregable1.Models
{
    public class Contacto
    {
        public string nombre { get; set; }
        public string telefono { get; set; }
        public string fechaNac { get; set; }
        public string comentario { get; set; }
        public Contacto(string nombre, string telefono, string fechaNac, string comentario)
        {
            this.nombre = nombre;
            this.telefono = telefono;
            this.fechaNac = fechaNac;
            this.comentario = comentario;
        }

    }
}