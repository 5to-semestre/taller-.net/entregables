﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Xml.Linq;

namespace Entregable1.Models
{
    public static class X_Document
    {
        private static string dirArch = $"{HostingEnvironment.MapPath("~")}/App_Data/contactos.xml";
        public static string getFilePath()
        {
            return dirArch;
        }
        public static XDocument getDoc()
        {
            XDocument document = new XDocument();
            if (!File.Exists(getFilePath()))
            {
                document.Add(
                    new XElement("contactos")
                );
                document.Save(getFilePath());
            }
            document = XDocument.Load(getFilePath());
            return document;
        }
        public static void agregar(Contacto c)
        {
            XDocument doc = getDoc();
            XElement contacto = new XElement("contacto");
            contacto.Add(new XElement("nombre", c.nombre));
            contacto.Add(new XElement("telefono", c.telefono));
            contacto.Add(new XElement("fechaNacimiento", c.fechaNac));
            contacto.Add(new XElement("comentario", c.comentario));
            doc.Root.Add(contacto);
            doc.Save(getFilePath());
        }
        public static List<Contacto> listar()
        {
            List<Contacto> listaC = new List<Contacto>();
            XDocument doc = getDoc();
            foreach(var nodo in doc.Root.Descendants("contacto"))
            {
                string nombre = nodo.Element("nombre").Value;
                string telefono = nodo.Element("telefono").Value;
                string fechaNac = nodo.Element("fechaNacimiento").Value;
                string comentario = nodo.Element("comentario").Value;
                listaC.Add(
                    new Contacto(nombre, telefono, fechaNac, comentario)
                );
            }
            return listaC;
        }
    }
}