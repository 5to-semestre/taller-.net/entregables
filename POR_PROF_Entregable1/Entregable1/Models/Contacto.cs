﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Entregable1.Models
{
    public class Contacto
    {
        public string Nombre { get; set; }
        public string Telefono { get; set; }
        public string FechaNacimiento { get; set; }
        public string Comentario { get; set; }
    }
}