﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Hosting;
using System.Xml;
using System.Xml.Linq;

namespace Entregable1.Models
{
    public class HandleContacto
    {
        private string fileName = "Contactos";

        private string GetFilePath()
        {
            return $"{ConfigurationManager.AppSettings.Get("DireccionXml")}/{fileName}.xml";
        }

        private XDocument GetDocumento()
        {
            XDocument document = new XDocument();

            if (!File.Exists(GetFilePath()))
            {
                // Creo nodo raiz
                document.Add(new XElement("contactos"));

                document.Save(GetFilePath());
            }

            document = XDocument.Load(GetFilePath());

            return document;
        }

        private void SaveDocumento(XDocument document)
        {
            document.Save(GetFilePath());
        }

        public void Crear(Contacto contacto)
        {

            XDocument document = GetDocumento();

            XElement xContacto = new XElement("contacto");

            xContacto.Add(new XElement("nombre", contacto.Nombre));
            xContacto.Add(new XElement("telefono", contacto.Telefono));
            xContacto.Add(new XElement("fechaNacimiento", contacto.FechaNacimiento));
            xContacto.Add(new XElement("comentario", contacto.Comentario));

            document.Root.Add(xContacto);

            SaveDocumento(document);

        }
        public List<Contacto> GetAll()
        {

            List<Contacto> contactos = new List<Contacto>();

            XDocument document = GetDocumento();

            foreach (var nodo in document.Root.Descendants("contacto"))
            {

                contactos.Add(new Contacto
                {
                    Comentario = nodo.Element("comentario").Value,
                    FechaNacimiento = nodo.Element("fechaNacimiento").Value,
                    Nombre = nodo.Element("nombre").Value,
                    Telefono = nodo.Element("telefono").Value,
                });

            }

            return contactos;


        }

    }
}