﻿using Entregable1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Entregable1.Controllers
{
    public class ContactoController : Controller
    {
        // GET: Contacto
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult NuevoComentario()
        {

            Contacto contacto = new Contacto()
            {
                Nombre = Request.Form["nombre"].ToString(),
                Telefono = Request.Form["telefono"].ToString(),
                FechaNacimiento = Request.Form["fechaNacimiento"].ToString(),
                Comentario = Request.Form["comentarios"].ToString()
            };

            HandleContacto handle = new HandleContacto();
            handle.Crear(contacto);

            return View();

        }

        public ActionResult ListadoComentarios()
        {

            HandleContacto handle = new HandleContacto();

            ViewData["contactos"] = handle.GetAll();

            return View();

        }



    }
}